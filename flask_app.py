# The code for the web API

from flask import Flask
from flask import jsonify

import requests
app = Flask(__name__)

@app.route('/')
def hello_world():
    return """
<script>
window.location.replace('https://gist.github.com/arjunj132/30969350853693e3fff16d9589677e15');
</script>
    """

@app.route('/search/<q>/')
def search(q):
    r = requests.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyC7HAr3ww-RvJii8vmOMTU6RNV8O0AQ8TE&cx=5733753a49edb83ba&q=' + q)
    return jsonify(r.json())

