# Create your own GoogAPI!
Step 1: Download [context.xml](https://bitbucket.org/arjunj132/googapi/raw/12d6ebd769adabd676ef6e4eb2d28d62fb539560/context.xml)

Step 2: Go to [Google CSE](https://cse.google.com)

Step 3: Create a new search engine without `google.com` as a site and set a title

Step 4: Click Create

Step 5: Click control panel

Step 6: Click advanced

Step 7: Click Search Engine Context

Step 8: Upload `context.xml`. The title should change to Google Search

Step 9: Go to basic

Step 10: Scroll down and click Custom Search JSON API Get Started. Then follow the directions!

---

You are done with your own GoogAPI clone!